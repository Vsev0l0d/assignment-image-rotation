#include <stdio.h>
#include "util.h"
#include "image.h"
#include "bmp.h"


void usage() {
    fprintf(stderr, "Usage: ./rotate SRC_IMG NEW_IMG\n");
}

int main(int argc, char **argv) {
    if (argc != 3) usage();
    if (argc > 3) err("Too many arguments \n");
    if (argc < 3) err("Not enough arguments \n");

    struct image image = {0};
    enum open_status open_status = read_image(argv[1], &image, from_bmp);
    if (open_status != OPEN_OK) err(open_errors_messages[open_status]);

    struct image rotated_image = {0};
    rotated_image =rotate(image);
    enum save_status save_status = save_image(argv[2], &rotated_image, to_bmp);
    if (save_status != SAVE_OK) err(save_errors_messages[save_status]);

    return 0;
}