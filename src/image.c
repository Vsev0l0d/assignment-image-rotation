#include <malloc.h>
#include "image.h"

enum open_status read_image(char const *filename, struct image *image, from_image_format from_format) {
    FILE *in = fopen(filename, "rb");
    if (!in) return OPEN_ERROR;
    enum read_status status = from_format(in, image);
    fclose(in);

    if (status != READ_OK) err(read_errors_messages[status]);

    return OPEN_OK;
}

struct image image_create(uint64_t width, uint64_t height) {
    return (struct image) {
            .width = width,
            .height = height,
            .data = malloc(sizeof(struct pixel) * width * height)
    };
}

void image_free(struct image *image) {
    free(image->data);
    image->data = NULL;
}

enum save_status save_image(char const *filename, struct image *image, to_image_format to_format) {
    FILE *out = fopen(filename, "wb");
    if (!out) return SAVE_ERROR;
    enum write_status status = to_format(out, image);
    fclose(out);

    if (status != WRITE_OK) err(write_errors_messages[status]);

    return SAVE_OK;
}


struct image rotate(struct image const source) {
    struct image rotated = image_create(source.height, source.width);

    for (int32_t c_height = 0; c_height < source.height; c_height++) {
        for (int32_t c_width = 0; c_width < source.width; c_width++) {
            rotated.data[source.height * c_width + (source.height - 1 - c_height)] = source.data[
                    source.width * c_height + c_width];
        }
    }
    return rotated;

}
