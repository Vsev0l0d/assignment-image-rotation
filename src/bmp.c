#include "bmp.h"
#include <stdbool.h>

#define BMP 20000

static uint32_t calc_padding(uint32_t width) {
    return (4 - width * 3 % 4) % 4;
}

static struct bmp_header bmp_header_create(uint32_t width, uint32_t height) {
    return (struct bmp_header) {
            .bfType = BMP,
            .bfileSize = width * height * 3 + calc_padding(width) * height + sizeof(struct bmp_header),
            .bfReserved = 0,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = 40,
            .biWidth = width,
            .biHeight = height,
            .biPlanes = 1,
            .biBitCount = 24,
            .biCompression = 0,
            .biSizeImage = width * height * 3 + calc_padding(width) * height,
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0
    };
}


static bool read_header(FILE *f, struct bmp_header *header) {
    return fread(header, sizeof(struct bmp_header), 1, f);
}

static bool write_header(FILE *f, struct bmp_header *header) {
    return fwrite(header, sizeof(struct bmp_header), 1, f);
}

enum read_status from_bmp(FILE *in, struct image *img) {
    struct bmp_header header = {};
    if (read_header(in, &header) != 1) return READ_INVALID_HEADER;
    if (header.bfType != BMP) return READ_INVALID_SIGNATURE;

    *img = image_create(header.biWidth, header.biHeight);
    uint32_t const padding = calc_padding(img->width);
    //read file
    for (uint32_t i = 0; i < img->height; i++) {
        int32_t r = fread(&img->data[i * img->width], sizeof(struct pixel), img->width, in);
        //check count of read bites
        if (r != img->width) {
            image_free(img);
            return READ_INVALID_BITS;
        }
        if (fseek(in, padding, SEEK_CUR) != 0) return READ_INVALID_PADDING;
    }
    return READ_OK;

}

enum write_status to_bmp(FILE *out, struct image const *img) {
    struct bmp_header header = bmp_header_create(img->width, img->height);
    if (write_header(out, &header) != 1) return WRITE_HEADER_ERROR;
    uint32_t const padding = calc_padding(img->width);
    int32_t padding_value = 0;

    //write data
    for (uint32_t i = 0; i < img->height; i++) {
        int32_t r = fwrite(&img->data[i * img->width], sizeof(struct pixel), img->width, out);
        if (r != img->width) {
            image_free((struct image *) img);
            return WRITE_DATA_ERROR;
        }
        fwrite(&padding_value, 1, padding, out);
    }
    return WRITE_OK;
}