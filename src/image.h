#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGE_H

#include <stdint.h>
#include <stdio.h>
#include "util.h"

struct pixel {
    uint8_t b, g, r;
};

struct image {
    uint64_t width, height;
    struct pixel *data;
};

struct image image_create(uint64_t width, uint64_t height);

void image_free(struct image *);

/* создаёт копию изображения, которая повёрнута на 90 градусов */
struct image rotate(struct image const source);

typedef enum read_status (*from_image_format)(FILE *file, struct image *image);

enum open_status read_image(char const *, struct image *, from_image_format);

typedef enum write_status (*to_image_format)(FILE *file, struct image const *image);

enum save_status save_image(char const *, struct image *, to_image_format);

#define ASSIGNMENT_IMAGE_ROTATION_IMAGE_H

#endif
